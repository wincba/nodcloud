![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/nod1.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E4%B8%80%E7%A7%92%E4%BA%92%E8%81%94%E4%B8%9A%E5%8A%A1%E5%9B%BE%E8%B0%B1-git.png)
# 点可云ERP-开源进销存系统
 :exclamation:  :exclamation:  :exclamation: 请注意：这是商业版本更新替换下来的。  纯上一个商业版本。

#   :heart:  :heart:  :heart:  如果你爱我，就给我点点 Starred ，  :pray: 

#### :fa-search-plus:  系统演示

#### V5开源 [V5开源](https://gitee.com/yimiaoOpen/nodcloud-v5)
[http://v5.yimiaonet.com/](http://v5.yimiaonet.com/)   帐号：admin   密码：admin888

#### V6开源 （当前项目）
[http://v6.yimiaonet.com/](http://v6.yimiaonet.com/)   帐号：admin   密码：admin888

###  :fa-check-square-o: 另有V7全新商业版
[https://erp.nodcloud.com/](https://erp.nodcloud.com/#/#git)   
V7全新商业版（V7版是付费的 售价299起） 采用tp+vue开发，速度更流畅，功能更齐全。 

###  :congratulations: V8版本开放公测，更多功能等你来探索，演示请直接扫码进群 :congratulations: 


#### :fa-weixin:  联系我们

官网qq群：[点可云软件中心](https://jq.qq.com/?_wv=1027&k=kJlp82a5)

官网客服qq：29139260   ，  （如有erp或其他网站，小程序，app开发需求可以联系）客服微信：diycloud

![扫码加入微信群](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E7%82%B9%E5%8F%AF%E4%BA%91%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)

扫描上方加入微信群，如二维码过期，添加下方客服微信 

![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/QQ%E6%88%AA%E5%9B%BE20220726111949.png)

####  :tw-1f18e: 安装方式一：
1、项目上传至服务器，
2、创建mysql数据库，
3、访问http://域名.com/install 输入数据库信息。
安装完成！！！

####  :tw-1f18e:  安装方式二：
1、打开宝塔面板，
2、点击宝塔面板左侧；软件商店，点击一键部署
3、在搜索栏搜索点可云，搜索出系统后 点击后面的一键部署输入域名即可。

如出现404错误，请配置伪静态：thinkphp

####  :cyclone:  :tw-2753: 视频安装教程：

点可云ERP教程 - PHPstudy安装篇
www.bilibili.com/video/BV1VT411Q7kV
点可云ERP教程 - 宝塔安装篇
www.bilibili.com/video/BV1As4y1Y7Ps
点可云ERP教程—关闭数据库严格模式
www.bilibili.com/video/BV1F54y1A7Vc


#### :fa-thumb-tack:  介绍
点可云进销存系统，基于thinkphp+layui开发。
功能包含：采购、销售、零售、多仓库管理、财务管理等功能 和超详细的报表功能（采购报表、销售报表、零售报表、仓库报表、资金报表等）

#### :fa-paperclip:  软件架构
thinkphp+layui

####  :fa-check-circle:   功能概览

```
购货
-购货单
-购货退货单
采购
-采购订单
-采购入库单
销货
-销货单
-销货退货单
零售
-零售单
-零售退货单
-服务单
-积分兑换单
仓库
-库存查询
-库存盘点
-库存预警
-调拨单
-其他入库单
-其他出库单
资金
-收款单
-付款单
-其他收入单
-其他支出单
-资金调拨单
报表
-单据核销单
-数据报表
设置
-基础资料
-辅助资料
-高级设置
```

![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220730161910.png)


#### :fa-smile-o:  安装教程

下载安装包，直接输入 http://www.你的网址.com/install/
然后输入你的数据库信息。 
> 如提示登录失败， 请配置伪静态  
至此 安装完成，请进行体验吧！

![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/image.png)


## :tw-1f50a:  开源版使用须知
版权信息
1.本项目（V6）版本可以用于商业用途，但仅限于保留版权的形式自用。
2.如对本项目（V6）版本二次销售的（：所有以本项目（V6）进行任何盈利性行为的），请务必联系官方授权。
3.（重要）未经授权禁止将本项目（V6）的代码和资源进行任何收费形式的传播，不得以本项目（V6）进行任何盈利性行为。否则我们将第一时间进行维权。

点可云公司，助力开源事业，打造纯粹的开源生态。
严厉打击破坏点可云开源生态的一切行为。

说明：
1.点可云v6版本不提供任何的技术支持和售后服务，如必要可联系我们付费处理，或您可以在社区内寻找解决方法。
2.如您商业使用，建议使用V7 V8商业版，可享受技术支持，售后服务，更新服务。

#### :clap:  系统截图

![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%A6%96%E9%A1%B5.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%9B%B6%E5%94%AE%E5%8D%95%E6%94%B6%E9%93%B6%E5%8F%B0.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%9B%B6%E5%94%AE1.png)
![输入链接说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%94%80%E8%B4%A7%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%87%87%E8%B4%AD%E8%AE%A2%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%87%87%E8%B4%AD%E5%85%A5%E5%BA%93%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E8%B4%AD%E8%B4%A7%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E5%AE%A2%E6%88%B7%E7%AE%A1%E7%90%86.png)




 :heart:  鸣谢：

点可云公司，一秒互联公司

## V7全新商业版功能介绍

![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/V7.png)


